\chapter{Числа с плавающей запятой}
Этот курс посвящен вычислениям на GPU с позиции наиболее естественной для него области знания -- с позиции трёхмерной графики.
\\
Начнётся он с проблемы номер один, всегда тихонько подкрадывающейся сзади, самой вездесущей и жестокой в нашем непростом кодерском деле -- это числа с плавающей запятой, везде далее называемые 'флоаты'.
\\
Ниже рассматриваются флоаты из стандарта \href[page=21]{./res/IEEE754-2008.pdf}{\color{teal}{IEEE 754}} -- те, которые с двоичной экспонентой и мантиссой.

\section{Представление беззнаковых целых}
Числа, которыми оперируют компьютеры, кодируют последовательностями битов.
Множество битов обозначим как $\mathbb{B} = \{0, 1\}$; тогда пространство n-битных слов является декартовым произведением $\mathbb{B}$ само на себя n раз, что естественно обозначить $\mathbb{B}^n$.
Например, элементом $\mathbb{B}^4$ является упорядоченная четвёрка бит $(0, 1, 0, 0)$, которую мы для краткости будем обозначать $0100$, начиная со старшего бита и заканчивая младшим.
\\
На множестве $\mathbb{B}^n$ можно ввести лексикографический порядок.
Тогда, каждый элемент $\mathbb{B}^n$ будет иметь порядковый номер от 0 до $2^n - 1$, который тривиально отождествляется с элементом кольца $\mathbb{Z}/2^n$
-- его мы и назовём n-битным беззнаковым числом.
Кольцо $\mathbb{Z}/n$ порождается взятием остатка от деления целого числа z на n: $mod\ n : \mathbb{Z} \rightarrow \mathbb{Z}/n$,
его элементы: $\{0, 1, ..., n - 1\}$.
\\
Короче говоря, лексикографический порядок индуцирует биекцию
\begin{equation*}
    ord : \mathbb{B}^n \rightarrow \mathbb{Z}/2^n
\end{equation*}
Она позволяет декодировать n-битное слово в n-битное беззнаковое число.
Например,
\begin{align*}
    ord(00000000) &= 0
    \\
    ord(01010101) &= 85
    \\
    ord(11111111) &= 255
\end{align*}
Обратная функция позволяет кодировать n-битные беззнаковые числа в n-битные слова:
\begin{equation*}
    ord^{-1}(85) = 01010101
\end{equation*}
а также проводить операции над ними:
\begin{align*}
    a + b &= ord^{-1}(ord(a) + ord(b))
    \\
    a \cdot b &= ord^{-1}(ord(a) \cdot ord(b))
\end{align*}
Например,
\begin{equation*}
    01100100 + 11001000 = ord^{-1}(100 + 200) = ord^{-1}(44) = 00101100
\end{equation*}

\section{Представление флоатов}
Наиболее распространённые 32-битные двоичные флоаты представлены 23-битной мантиссой $\overline{m}_{23} \in \mathbb{B}^{23}$, 8-битной экспонентой $\overline{e}_8 \in \mathbb{B}^8$, и одним битом знака $\overline{s} \in \mathbb{B}$.
Обозначим $s = ord(\overline{s})$, $e_8 = ord(\overline{e}_8)$, $m_{23} = ord(\overline{m}_{23})$.
Тогда, если $e_8 \neq 0$ и $e_8 \neq 2^8 - 1$, 32-битный флоат $f_{32} \in \mathbb{F}_{32}$ представляется как
\begin{equation}
    f_{32}(s, e_8, m_{23}) = (-1)^s \cdot 2^{e_8 - 127} \cdot (1 + m_{23} \cdot 2^{-23})
\end{equation}
Такие числа по модулю варьируются от
\begin{equation}
    f_{32}^{min} = f_{32}(0, 1, 0) = 2^{-126} \approx 1.1754943 \cdot 10^{-38}
\end{equation}
до
\begin{equation}
    f_{32}^{max} = f_{32}(0, 254, 2^{23} - 1) = 2^{127} \cdot (2 - 2^{-23}) \approx 3.4028234 \cdot 10^{38}
\end{equation}
В случае, если $e_8 = 0$, число $f_{32}$ называется денормализованным, и представляется как
\begin{equation}
    f_{32}(s, e_8 = 0, m_{23}) = f_{32}^{denorm}(s, m_{23}) = (-1)^s \cdot 2^{-126} \cdot m_{23} \cdot 2^{-23}
\end{equation}
Денормализованные числа равномерно заполняют пространство от $-2^{-126}$ до $2^{-126}$.
Ближайшее по модулю к нулю денормализованное число равно
\begin{equation}
    f_{32}(0, 0, 1) = 2^{-149} \approx 1.4012984 \cdot 10^{-45}
\end{equation}
К денормализованным числам также относятся два нуля: $f_{32}(0, 0, 0) = +0$ и $f_{32}(1, 0, 0) = -0$.
\\
Флоаты, у которых $e_8 = 2^8 - 1$ и $m_{23} \neq 0$, называются NaN (not a number). Если же $m_{23} = 0$, то это +inf или -inf, в зависимости от знака.
\\
Нетрудно посчитать, что значащих 32-битных флоатов всего $2^{32} - 2^{24} + 2$ штук -- количество 32-битных слов за вычетом NaN-ов.
\\
Поиграться с битами вручную можно \href{https://evanw.github.io/float-toy/}{\color{blue}{тут}}.

\section{Операции}
Зная, как представлены флоаты, нетрудно представить, как их складывать
\begin{equation}
    f_1 + f_2 = (-1)^{s_1} \cdot 2^{e_1 - 127} \cdot (1 + m_1 \cdot 2^{-23} + (-1)^{s_2 - s_1} \cdot 2^{e_2 - e_1} \cdot (1 + m_2 \cdot 2^{-23})), \quad e_2 < e_1;
\end{equation}
и умножать:
\begin{equation}
    f_1 \cdot f_2 = (-1)^{s_1 + s_2} \cdot 2^{e_1 + e_2 - 254} \cdot (1 + (m_1 + m_2) \cdot 2^{-23} + m_1 \cdot m_2 \cdot 2^{-46})
\end{equation}
Нетрудно заметить, что точные результаты сложения и умножения сами по себе непредставимы как флоаты.
Но результат сложения или умножения нужно сделать флоатом, поэтому он округляется до соседнего флоата по одному из пяти правил \href[page=28]{./res/IEEE754-2008.pdf}{\color{teal}{(4.3.1), (4.3.2)}}.
Конкретный пример того, как округление ломает ассоциативность сложения, есть \href[page=2]{./res/cuda-fp.pdf}{\color{teal}{вот тут (2.2)}}.
\\
Сложение флоатов является большой проблемой потому, что числа с разной экспонентой вносят разный вклад в результат.
Мантисса числа с большей экспонентой учитывается полноценно, а мантисса числа с меньшей экспонентой обрезается на количество бит, соответствующее разнице экспонент.
Так, довольно просто можно получить ситуацию, когда \href{https://gcc.godbolt.org/#g:!((g:!((g:!((h:codeEditor,i:(filename:'1',fontScale:14,fontUsePx:'0',j:1,lang:c%2B%2B,selection:(endColumn:1,endLineNumber:18,positionColumn:1,positionLineNumber:18,selectionStartColumn:1,selectionStartLineNumber:18,startColumn:1,startLineNumber:18),source:'%23include+%3Ciostream%3E%0A%23include+%3Ccmath%3E%0A%23include+%3Cbit%3E%0A%0Ausing+i32+%3D+int%3B%0Ausing+f32+%3D+float%3B%0A%0Aint+main()%0A%7B%0A++++f32+a+%3D+1.f%3B%0A++++while(1.f+%2B+a+!!%3D+1.f)%0A++++++++a+*%3D+0.5f%3B%0A++++std::cout+%3C%3C+a+%3C%3C+std::endl%3B%0A%0A++++i32+const+e+%3D+(std::bit_cast%3Ci32%3E(a)+%3E%3E+23)+-+127%3B%0A++++std::cout+%3C%3C+e+%3C%3C+std::endl%3B%0A++++std::cout+%3C%3C+std::pow(2.f,+e)+%3C%3C+std::endl%3B%0A%7D'),l:'5',n:'0',o:'C%2B%2B+source+%231',t:'0')),k:52.621101526211014,l:'4',n:'0',o:'',s:0,t:'0'),(g:!((h:executor,i:(argsPanelShown:'1',compilationPanelShown:'0',compiler:g123,compilerName:'',compilerOutShown:'0',execArgs:'',execStdin:'',fontScale:14,fontUsePx:'0',j:1,lang:c%2B%2B,libs:!(),options:'-std%3Dc%2B%2B20+-O3',overrides:!(),source:1,stdinPanelShown:'0',wrap:'1'),l:'5',n:'0',o:'Executor+x86-64+gcc+12.3+(C%2B%2B,+Editor+%231)',t:'0')),header:(),k:47.378898473788986,l:'4',n:'0',o:'',s:0,t:'0')),l:'2',n:'0',o:'',t:'0')),version:4}{\color{blue}{$1 + a = 1$}}: это происходит уже при $a = 2^{-24}$.
Поэтому, при сложении флоатов, постоянно приходится задумываться о том, насколько они близки по модулю.
\\
Умножение флоатов, в отличие от сложения, ассоциативно, потому что мантиссы учитыватся равноправно.
Поэтому, можно перемножать очень маленькие по модулю числа с очень большими и не переживать.
В то же время, у каждого флоата есть обратный по сложению, но не у каждого есть обратный по умножению -- поэтому, $x / y$ и $x \cdot (1 / y)$ могут иметь разные значения.

\section{Float как поле скаляров}
Полем называется алгебра над множеством $F$, образующая коммутативную группу по сложению над $F$ и умножению над $F \setminus \{0\}$ с дистрибутивностью умножения относительно сложения.
\\
Символьно, $(+) :: F \times F \rightarrow F; \quad (*) :: F \times F \rightarrow F$, так, что
\begin{align*}
    (assoc    \quad +):    &\quad \forall a, b, c \in F,               &(a + b) + c = a + (b + c)
    \\
    (commute  \quad +):    &\quad \forall a, b \in F,                  &a + b = b + a
    \\
    (identity \quad +):    &\quad \exists !0 \in F: \forall a \in F,   &a + 0 = 0 + a = a
    \\
    (inverse  \quad +):    &\quad \forall a \in F, \exists !(-a) \in F:&a + (-a) = (-a) + a = 0
    \\
    (assoc    \quad \cdot):&\quad \forall a, b, c \in F,                                                  &(a \cdot b) \cdot c = a \cdot (b \cdot c)
    \\
    (commute  \quad \cdot):&\quad \forall a, b \in F,                                                     &a \cdot b = b \cdot a
    \\
    (identity \quad \cdot):&\quad \exists !1 \in F: \forall a \in F,                                      &a \cdot 1 = 1 \cdot a = a
    \\
    (inverse  \quad \cdot):&\quad \forall a \in F \setminus \{0\}, \exists !a^{-1} \in F \setminus \{0\}: &a \cdot a^{-1} = a^{-1} \cdot a = 1
    \\
    (distribution)        :&\quad \forall a, b, c \in F,                                                  &(a + b) \cdot c = (a \cdot c) + (b \cdot c)
\end{align*}
Как показано выше, флоаты не образуют поле.
Конкретно, не имеют места \href{https://gcc.godbolt.org/#g:!((g:!((g:!((h:codeEditor,i:(filename:'1',fontScale:14,fontUsePx:'0',j:1,lang:c%2B%2B,selection:(endColumn:1,endLineNumber:12,positionColumn:1,positionLineNumber:12,selectionStartColumn:1,selectionStartLineNumber:12,startColumn:1,startLineNumber:12),source:'%23include+%3Ciostream%3E%0A%23include+%3Cbit%3E%0A%0Ausing+u32+%3D+unsigned+int%3B%0Ausing+f32+%3D+float%3B%0A%0Aint+main()%0A%7B%0A++++u32+good+%3D+0u%3B%0A++++u32+const+M+%3D+(1u+%3C%3C+23u)%3B%0A++++f32+const+b+%3D+1.f%3B%0A++++f32+const+c+%3D+3.1415926535f%3B%0A++++for(u32+m+%3D+0u%3B+m+%3C+M%3B+%2B%2Bm)%0A++++%7B%0A++++++++f32+const+a+%3D+std::bit_cast%3Cf32%3E(0x3f800000u+%7C+m)%3B%0A++++++++good+%2B%3D+((a+%2B+b)+%2B+c+%3D%3D+a+%2B+(b+%2B+c))+%3F+1u+:+0u%3B+%0A++++%7D%0A++++std::cout+%3C%3C+%22++++associative:+%22+%3C%3C+++++good+%3C%3C+std::endl%3B%0A++++std::cout+%3C%3C+%22not+associative:+%22+%3C%3C+M+-+good+%3C%3C+std::endl%3B%0A%7D'),l:'5',n:'0',o:'C%2B%2B+source+%231',t:'0')),k:52.621101526211014,l:'4',n:'0',o:'',s:0,t:'0'),(g:!((h:executor,i:(argsPanelShown:'1',compilationPanelShown:'0',compiler:g123,compilerName:'',compilerOutShown:'0',execArgs:'',execStdin:'',fontScale:14,fontUsePx:'0',j:1,lang:c%2B%2B,libs:!(),options:'-std%3Dc%2B%2B20+-O3',overrides:!(),source:1,stdinPanelShown:'0',wrap:'1'),l:'5',n:'0',o:'Executor+x86-64+gcc+12.3+(C%2B%2B,+Editor+%231)',t:'0')),header:(),k:47.378898473788986,l:'4',n:'0',o:'',s:0,t:'0')),l:'2',n:'0',o:'',t:'0')),version:4}{\color{blue}{ассоциативность сложения}},
\href{https://gcc.godbolt.org/#g:!((g:!((g:!((h:codeEditor,i:(filename:'1',fontScale:14,fontUsePx:'0',j:1,lang:c%2B%2B,selection:(endColumn:43,endLineNumber:17,positionColumn:43,positionLineNumber:17,selectionStartColumn:43,selectionStartLineNumber:17,startColumn:43,startLineNumber:17),source:'%23include+%3Ciostream%3E%0A%23include+%3Cbit%3E%0A%0Ausing+u32+%3D+unsigned+int%3B%0Ausing+f32+%3D+float%3B%0A%0Aint+main()%0A%7B%0A++++u32+good+%3D+0u%3B%0A++++u32+const+M+%3D+(1u+%3C%3C+23u)%3B%0A++++for(u32+m+%3D+0u%3B+m+%3C+M%3B+%2B%2Bm)%0A++++%7B%0A++++++++f32+const+f+%3D+std::bit_cast%3Cf32%3E(0x3f800000u+%7C+m)%3B%0A++++++++f32+const+finv+%3D+1.f+/+f%3B%0A++++++++good+%2B%3D+(1.f+%3D%3D+f+*+finv)+%3F+1u+:+0u%3B+%0A++++%7D%0A++++std::cout+%3C%3C+%22++++invertible:+%22+%3C%3C+++++good+%3C%3C+std::endl%3B%0A++++std::cout+%3C%3C+%22not+invertible:+%22+%3C%3C+M+-+good+%3C%3C+std::endl%3B%0A%7D'),l:'5',n:'0',o:'C%2B%2B+source+%231',t:'0')),k:52.621101526211014,l:'4',n:'0',o:'',s:0,t:'0'),(g:!((h:executor,i:(argsPanelShown:'1',compilationPanelShown:'0',compiler:g123,compilerName:'',compilerOutShown:'0',execArgs:'',execStdin:'',fontScale:14,fontUsePx:'0',j:1,lang:c%2B%2B,libs:!(),options:'-std%3Dc%2B%2B20+-O3',overrides:!(),source:1,stdinPanelShown:'0',wrap:'1'),l:'5',n:'0',o:'Executor+x86-64+gcc+12.3+(C%2B%2B,+Editor+%231)',t:'0')),header:(),k:47.378898473788986,l:'4',n:'0',o:'',s:0,t:'0')),l:'2',n:'0',o:'',t:'0')),version:4}{\color{blue}{существование обратного по умножению}}
и \href{https://gcc.godbolt.org/#g:!((g:!((g:!((h:codeEditor,i:(filename:'1',fontScale:14,fontUsePx:'0',j:1,lang:c%2B%2B,selection:(endColumn:1,endLineNumber:18,positionColumn:1,positionLineNumber:18,selectionStartColumn:1,selectionStartLineNumber:18,startColumn:1,startLineNumber:18),source:'%23include+%3Ciostream%3E%0A%23include+%3Cbit%3E%0A%0Ausing+u32+%3D+unsigned+int%3B%0Ausing+f32+%3D+float%3B%0A%0Aint+main()%0A%7B%0A++++u32+good+%3D+0u%3B%0A++++u32+const+M+%3D+(1u+%3C%3C+23u)%3B%0A++++f32+const+c+%3D+3.1415926535f%3B%0A++++f32+const+b+%3D+1.f%3B%0A++++for(u32+m+%3D+0u%3B+m+%3C+M%3B+%2B%2Bm)%0A++++%7B%0A++++++++f32+const+a+%3D+std::bit_cast%3Cf32%3E(0x3f800000u+%7C+m)%3B%0A++++++++good+%2B%3D+((a+%2B+b)+*+c+%3D%3D+a+*+c+%2B+b+*+c)+%3F+1u+:+0u%3B+%0A++++%7D%0A++++std::cout+%3C%3C+%22++++distributive:+%22+%3C%3C+++++good+%3C%3C+std::endl%3B%0A++++std::cout+%3C%3C+%22not+distributive:+%22+%3C%3C+M+-+good+%3C%3C+std::endl%3B%0A%7D'),l:'5',n:'0',o:'C%2B%2B+source+%231',t:'0')),k:52.621101526211014,l:'4',n:'0',o:'',s:0,t:'0'),(g:!((h:executor,i:(argsPanelShown:'1',compilationPanelShown:'0',compiler:g123,compilerName:'',compilerOutShown:'0',execArgs:'',execStdin:'',fontScale:14,fontUsePx:'0',j:1,lang:c%2B%2B,libs:!(),options:'-std%3Dc%2B%2B20+-O3',overrides:!(),source:1,stdinPanelShown:'0',wrap:'1'),l:'5',n:'0',o:'Executor+x86-64+gcc+12.3+(C%2B%2B,+Editor+%231)',t:'0')),header:(),k:47.378898473788986,l:'4',n:'0',o:'',s:0,t:'0')),l:'2',n:'0',o:'',t:'0')),version:4}{\color{blue}{дистрибутивность}}.
Со всеми остальными тождествами полный порядок и ими можно пользоваться.

\section{vec3}
Введём пространство $\mathbb{F}^3$, элементами которого являются упорядоченные тройки чисел с плавающей запятой:
\begin{lstlisting}
struct vec3
{
    float x, y, z;
};
\end{lstlisting}
Флоаты не образуют поле, поэтому $\mathbb{F}^3$ не образуют векторное пространство.
Тем не менее, работая с флоатами, мы прикидываемся, что работаем с действительными числами -- поэтому, работая с $\mathbb{F}^3$, точно так же будем прикидываться, что это векторное пространство.
По аналогии с $\mathbb{R}^3$, нетрудно ввести сложение
\begin{lstlisting}
inline vec3 operator+(vec3 const &a, vec3 const &b) noexcept
{
    return {a.x + b.x, a.y + b.y, a.z + b.z};
}
\end{lstlisting}
умножение на скаляр
\begin{lstlisting}
inline vec3 operator*(vec3 const &a, float const f) noexcept
{
    return {a.x * f, a.y * f, a.z * f};
}
\end{lstlisting}
и скалярное произведение
\begin{lstlisting}
inline float dot(vec3 const &a, vec3 const &b) noexcept
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}
\end{lstlisting}
Естественно, реальность будет нам регулярно напоминать о том, что $\mathbb{F}^3$ не является евклидовым пространством.
Оно дискретно, в нём нет привычной коллинеарности или компланарности, не получится нормировать вектор.
\\
Тем не менее, мы обычно будем заниматься математикой в $\mathbb{R}^3$, а потом переносить её на $\mathbb{F}^3$.
