\chapter{Изображение трёхмерной сцены}

\section{Цвет}
Только в 1931 году, цвет получил чёткое количественное определение: были введены цветовые пространства CIE RGB и XYZ, на которые можно проецировать энергетический спектр электромагнитного излучения.
Количественную характеристику цвету удалось задать, исходя из предположения о том, что цвет образует область в трёхмерном линейном пространстве.
Идея о том, что пространство цветов трёхмерно, основывается на наблюдении о наличии в человеческом глазе трёх видов цветочувствительных "колбочек".
\begin{figure}[H]
    \centering
    \includegraphics[width=.7\textwidth]{./res/cones.png}
    \caption{Нормированные функции восприятия колбочек}
\end{figure}
Предполагалось, что наблюдаемые глазом цвета можно воссоздать как линейную комбинацию трёх источников монохромного света,
и что коэффициенты разложения окажутся для разных людей примерно одинаковыми.
Для экспериментов, были выбраны источники света с длиной волны 435.8, 546.1 и 700 нанометров.
По данным экспериментов подогнали функции восприятия цвета ``стандартного наблюдателя'' $\overline{r}(\lambda),\ \overline{g}(\lambda),\ \overline{b}(\lambda)$, с помощью которых спектральная плотность излучения $S(\lambda)$ раскладывается на компоненты $R, G, B$:
\begin{equation}
    R = \int_0^{+\infty} S(\lambda) \overline{r}(\lambda) d\lambda, \quad
    G = \int_0^{+\infty} S(\lambda) \overline{g}(\lambda) d\lambda, \quad
    B = \int_0^{+\infty} S(\lambda) \overline{b}(\lambda) d\lambda, \quad
\end{equation}
\begin{figure}[H]
    \centering
    \includegraphics[width=.65\textwidth]{./res/rgb.png}
    \caption{Нормированные функции $\overline{r}, \overline{g}, \overline{b}$}
\end{figure}
Оказалось, что некоторые видимые цвета оказались непредставимы через неотрицательные $R, G, B$.
Поэтому, посредством \href[page=7]{./res/xyz.pdf}{\color{teal}{линейного преобразования}} были введены компоненты $X, Y, Z$, лишённые этого недостатка.
Соответствующие им функции $\overline{x}, \overline{y}, \overline{z}$ неотрицательны:
\begin{figure}[H]
    \centering
    \includegraphics[width=.7\textwidth]{./res/xyz.png}
    \caption{Функции $\overline{x}, \overline{y}, \overline{z}$}
\end{figure}
Аппроксимации этих функций можно подсмотреть \href[page=4]{./res/xyz_approx.pdf}{\color{teal}{вот тут}}.
\\
Кроме этого, функция $\overline{y}$ была выбрана пропорциональной \href{https://en.wikipedia.org/wiki/Luminous_efficiency_function}{\color{blue}{функции}} спектральной световой эффективности монохроматического излучения, с помощью которой радиометрические величины переводятся в фотометрические -- иначе говоря, яркость света определяется компонентой $Y$ и остаётся неизменной при изменении двух других компонент.
Переход к координатам цветности $x = \frac{X}{X + Y + Z}, y = \frac{Y}{X + Y + Z}$ даёт понятную проекцию пространства цветов на плоскость:
\begin{figure}[H]
    \centering
    \includegraphics[width=.7\textwidth]{./res/chroma.png}
    \caption{Все видимые цвета, треугольник CIE RGB и точка однородного спектра E = $(\frac{1}{3}, \frac{1}{3})$}
\end{figure}
В линейном цветовом пространстве удобно преобразовывать цвет, но, как оказалось, неудобно хранить.
Для хранения, компоненты цвета обычно приходится ужимать в ограниченный диапазон, например, от 0 до 1.
В силу логарифмической чувствительности глаза к интенсивности света, линейно упакованный цвет \href{https://www.shadertoy.com/view/XdX3D4}{\color{blue}{сильно страдает от дискретизации в диапазоне тёмных цветов}}.
Поэтому, линейное цветовое пространство $[0, 1]^3$ преобразуют в ``линейное по воспринимаемой интенсивности'' по \href[page=18]{./res/rgb.pdf}{\color{teal}{формулам, напоминающим $x \rightarrow \sqrt{x}$}}.
Для того, чтобы привести компоненты цвета от $[0, +\infty]$ к $[0, 1]$, существуют \href{https://www.shadertoy.com/view/lslGzl}{\color{blue}{tone mapping функции}}.
\\
В любом случае, о том, как корректно отображать цвет, заботятся производители экранов -- от нас требуется только упаковать компоненты цвета в правильном цветовом пространстве.

\section{Изображение}
С изображениями формата \href{https://en.wikipedia.org/wiki/Netpbm}{netbpm} легко работать на любом языке программирования.
Например, на С++ можно без труда сгенерировать красно-зелёный градиент:

\begin{lstlisting}
using u32 = unsigned int;
using f32 = float;

std::ofstream file("out.ppm");
u32 const width  = 1920u;
u32 const height = 1080u;
file << "P3\n" << width << ' ' << height << "\n255\n";

for(u32 y = 0u; y < height; ++y)
for(u32 x = 0u; x <  width; ++x)
{
    f32 const fx = f32(x) / f32( width);
    f32 const fy = f32(y) / f32(height);

    file << u32(255.f * fx) << ' ' << u32(255.f * fy) << ' ' << 0u << ' ';
}
\end{lstlisting}

Цвет в этом формате хранится в нелинейном пространстве sRGB по 8 бит на компоненту, от 0 до 255.
Поэтому, приведённый выше код генерирует градиент, линейный по воспринимаемой интенсивности.
Чтобы получить градиент, линейный в sRGB, нужно скорректировать цвет:

\begin{lstlisting}
inline f32 encodeSRGB(f32 const x) noexcept
{
    return x < 0.0031308f
        ? 12.92f * x
        : 1.055f * std::pow(x, 1.f / 2.4f) - 0.055f;
}
\end{lstlisting}

\begin{figure}[H]
    \centering
    \includegraphics[width=.45\textwidth]{./res/perceptual.png}
    \includegraphics[width=.45\textwidth]{./res/physical.png}
    \caption{Градиент до коррекции и после неё.}
\end{figure}

\section{Камера}
Камера, по сути, является объединением множества сенсоров и оптической системы, фокусирующей пучки лучей на эти сенсоры.
Для каждого сенсора определена чувствительность, которая зависит от конкретного луча и переносимого им света.
\\
Для начала, рассмотрим простейшую камеру, состоящую из двумерного массива пикселей.
Пиксели реагируют только на свет, приходящий вдоль лучей, пущенных из точки на оси прямоугольника, origin, на их центры:
\begin{figure}[H]
    \centering
    \includegraphics[width=.7\textwidth]{./res/camera.png}
    \caption{Простейшая точечная камера}
\end{figure}
Такая камера чаще всего параметризуется точкой камеры $\vec{origin}$, точкой, куда направлена камера $\vec{at}$, направлением вверх $\vec{up}$,
вертикальным углом раствора $\varphi$ и соотношением сторон прямоугольника $ratio$.
В этой параметризации, ортонормированная тройка векторов рассчитывается так:
\begin{align*}
\vec{z} &= normalize(\vec{origin} - \vec{at})
\\
\vec{x} &= normalize([\vec{up}, \vec{z}])
\\
\vec{y} &= [\vec{z}, \vec{x}]
\end{align*}
Прямоугольник камеры обычно параметризуется координатами $(u, v) \in [-1; 1] \times [-1; 1]$, из которых направление луча рассчитывается как
\begin{align*}
\vec{d} &= \vec{x} * u * tan(\varphi) * ratio
\\
        &+ \vec{y} * v * tan(\varphi)
\\
        &- \vec{z}
\end{align*}

\newpage
\section{Сцена}
Сценой будем называть всё то, что влияет на приходящий в камеру свет.
Это могут быть источники света или рассеивающие свет поверхности или объёмы.
Чтобы получить примитивное изображение сцены через нашу простейшую камеру, достаточно:
\begin{itemize}
    \item{выстрелить из каждого пикселя лучом}
    \item{найти пересечение луча с поверхностью сцены}
    \item{посчитать свет, падающий на найденную точку пересечения луча со сценой}
\end{itemize}
Для примера, получим изображение простейшей сцены из четырёх сфер с одним источником света.

Соберём из сфер разноцветного снеговика и землю:
\begin{lstlisting}
Sphere const sphere[4] =
{
    {
        .origin = {0.f, 0.1f, 0.f},
        .radius = 0.5f,
    },
    {
        .origin = {0.f, 0.7f, 0.f},
        .radius = 0.3f,
    },
    {
        .origin = {0.f, -0.4f, 0.f},
        .radius = 0.7f,
    },
    {
        .origin = {0.f, -1000.f, 0.f},
        .radius = 999.5f,
    },
};
vec3 const albedo[4] =
{
    {0.8f, 0.3f, 0.3f},
    {0.3f, 0.3f, 0.8f},
    {0.8f, 0.8f, 0.8f},
    {0.2f, 0.5f, 0.2f},
};
\end{lstlisting}
Функцию, возвращающую ближайшее пересечение со сферами, можно реализовать примерно вот так:
\begin{lstlisting}
struct Hit
{
    u32 sphereI;
    f32 t;
};
auto const closestHit = [sphere](Ray const ray, RayRange const range) noexcept
    -> std::optional<Hit>
{
    return std::ranges::fold_left
    (
        sphere | std::views::enumerate
               | std::views::transform([=](auto const &pair) noexcept
            -> std::optional<Hit>
        {
            auto const &[i, s] = pair;
            auto const rsi = raySphereIntersection(ray, s);
            return nonempty(rsi + range)
                ? std::optional<Hit>
                {{
                    .sphereI = u32(i),
                    .t = contains(range, rsi.tMin) ? rsi.tMin : rsi.tMax,
                }}
                : std::nullopt;
        }),
        std::nullopt,
        [](std::optional<Hit> const &accum, std::optional<Hit> const &hit) noexcept
        {
            if(!accum)
                return hit;
            if(!hit)
                return accum;
            return (accum->t < hit->t) ? accum : hit;
        }
    );
};
\end{lstlisting}

Остаётся только подкрасить луч
\begin{lstlisting}
auto const trace = [&](Ray const ray) noexcept
    -> vec3
{
    vec3 const   skyColor = {0.53f, 0.81f, 0.92f};
    vec3 const lightColor = {1.00f, 0.98f, 0.88f};
    vec3 const lightDir   = normalize({3.f, 3.f, -1.f});

    auto const hit = closestHit(ray, zeroToInf);
    if(!hit)
        return skyColor;

    auto const [i, t] = *hit;
    vec3 const pos = ray.origin + ray.direction * t;
    vec3 const norm = normalize(pos - sphere[i].origin);
    float const NL = std::max(0.f, dot(norm, lightDir));

    auto const shadowHit = closestHit({pos, lightDir}, {2e-4f, 1.f / 0.f});
    return albedo[i] * (skyColor * 0.1f + lightColor * NL * (shadowHit ? 0.f : 0.7f));
};
\end{lstlisting}

и собрать изображение
\begin{lstlisting}
std::ofstream file("out.ppm");
u32 const width  = 1920u;
u32 const height = 1080u;
file << "P3\n" << width << ' ' << height << "\n255\n";

Camera const camera =
{
    .origin = {0.f, 0.8f, -2.f},
    .at = {0.f, 0.f, 0.f},
    .up = {0.f, 1.f, 0.f},
    .fov = 0.55f,
    .aspectRatio = f32(width) / f32(height),
};

for(u32 y = 0u; y < height; ++y)
for(u32 x = 0u; x <  width; ++x)
{
    f32 const u = -1.f + 2.f * (0.5f + f32(x)) / f32( width);
    f32 const v =  1.f - 2.f * (0.5f + f32(y)) / f32(height);
    auto const [r, g, b] = trace(camera.castRay(u, v));

    auto const encode = +[](f32 const f) noexcept
    {
        f32 const c = f < 0.0031308f
            ? f * 12.92f
            : 1.055f * std::pow(f, 1.f / 2.4f) - 0.055f;
        return u32(std::round(255.f * c));
    };
    file << encode(r) << ' '
         << encode(g) << ' '
         << encode(b) << ' ';
}
\end{lstlisting}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{./res/snowman.png}
    \caption{Результат}
\end{figure}
