#pragma once
#include "squares.h"
#include <utils/types.h>
#include <numbers>

struct HemisphereSample
{
    vec3 direction;
    f32 pdf;
};

inline HemisphereSample uniformHemisphereSample() noexcept
{
    f32 const cosTheta = generateUniformFloat();
    f32 const sinTheta = std::sqrt(1.f - cosTheta * cosTheta);

    f32 const twopi = 2.f * std::numbers::pi_v<f32>;
    f32 const phi = twopi * generateUniformFloat();
    return
    {
        .direction =
        {
            sinTheta * std::cos(phi),
            sinTheta * std::sin(phi),
            cosTheta,
        },
        .pdf = 1.f / twopi,
    };
}

struct ProjectedHemisphereSample
{
    vec3 direction;
    f32 pdf;
};

inline HemisphereSample uniformProjectedHemisphereSample() noexcept
{
    for(;;)
    {
        f32 const x = -1.f + 2.f * generateUniformFloat();
        f32 const y = -1.f + 2.f * generateUniformFloat();
        if(x * x + y * y > 1.f)
            continue;
        return
        {
            .direction =
            {
                x,
                y,
                std::sqrt(1.f - x * x - y * y)
            },
            .pdf = 1.f / std::numbers::pi_v<f32>,
        };
    }
}

struct EmissiveTriangleInfo {
    gltf::gvec<vec3, 3> pos;
    vec3 emission;
    u32 instanceI;
    u32 triangleI;
};

struct IndexSample
{
    u32 i;
    f32 probability;
};

template<std::ranges::range R>
auto indexSamplerFrom(R &&weights) noexcept
{
    std::vector<f32> w = {0.f};
    for(auto const y : weights)
        w.push_back(y + w.back());
    return [c = 1.f / w.back(), weight = std::move(w)]() noexcept
    {
        auto const it = std::ranges::upper_bound
                (
                        weight,
                        generateUniformFloat() * weight.back()
                );
        u32 const i = u32(it - weight.begin()) - 1u;
        return IndexSample{i, c * (weight[i + 1] - weight[i])};
    };
}

struct TriangleSample
{
    vec3 p;
    f32 pdf;
};

inline TriangleSample uniformTrianglePoint( gltf::gvec<vec3, 3> const &triangle
                                            , vec3 const rayOrigin
                                            , vec3 const rayOriginNorm
                                            )
{
    auto const [r0, r1, r2] = triangle;
    vec3 const e1 = r1 - r0;
    vec3 const e2 = r2 - r0;
    for(;;) {
        f32 const p = generateUniformFloat();
        f32 const q = generateUniformFloat();
        if(p + q > 1.f)
            continue;
        vec3 const point = r0 + p * e1 + q * e2;
        vec3 const dr = point - rayOrigin;
        auto S = dot(dr, cross(r1 - r0, r2 - r0)) / 2.f;
        return {
            .p = point,
            .pdf = dot(dr, dr) * dot(dr, dr) /
                    std::abs(dot(rayOriginNorm, dr) * S)
        };
    }
}

inline HemisphereSample MySample(const std::vector<EmissiveTriangleInfo> & triangles,
                                 IndexSample indexSample, const vec3 & pos, const vec3 & norm) noexcept {

    auto triangleSample =
            uniformTrianglePoint(triangles[indexSample.i].pos, pos, norm);

    return {
        .direction = triangleSample.p,
        .pdf = triangleSample.pdf * indexSample.probability
    };
}
